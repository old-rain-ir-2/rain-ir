/*!
Evaluation and normalization of `rain` values
*/
use crate::error::Error;
use crate::repr::*;
use crate::typing::*;
use crate::value::*;
use fxhash::FxHashMap;
use pour::*;
use std::borrow::Borrow;
use std::hash::{Hash, Hasher};
use std::ops::Deref;

mod app;
mod ctx;
pub use app::*;
pub use ctx::*;

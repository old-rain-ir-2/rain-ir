/*!
Evaluation contexts
*/
use super::*;

/// An evaluation context descriptor
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct EvalDesc {
    /// The symbol mapping of this context
    symbols: IdMap<SymbolId, ValId>,
    /// Whether this context should normalize values in substitution
    normalize: bool,
    /// Whether this context should use the cache
    should_cache: bool,
    /// The universe delta of this evaluation
    universe_delta: u64,
}

impl EvalDesc {
    /// Substitute a value for a symbol. Return a type error on failure, and a new descriptor (if necessary) on success.
    ///
    /// # Examples
    /// ```rust
    /// # use rain_ir::eval::EvalDesc;
    /// # use rain_ir::value::*;
    /// # use rain_ir::primitive::nats::*;
    /// # use rain_ir::typing::*;
    /// # use rain_ir::repr::*;
    /// let zero = Natural::from(0u128).into_val();
    /// let nat_var = SymbolId::new(Nats.into_repr());
    /// let empty = EvalDesc::default();
    /// let (match_, zero_desc) = empty.inserted(nat_var.clone(), zero)
    ///     .unwrap();
    /// let zero_desc = zero_desc.unwrap();
    /// assert_eq!(match_, TypeMatch::default());
    /// assert_eq!(zero_desc.len(), 1);
    /// assert_ne!(empty, zero_desc);
    /// assert!(
    ///     empty.inserted(nat_var.clone(), TYPE.clone_as_val())
    ///         .is_err()
    /// );
    /// ```
    pub fn inserted(
        &self,
        symbol: SymbolId,
        value: ValId,
    ) -> Result<(Match, Option<EvalDesc>), Error> {
        self.inserted_in(symbol, value, &mut ())
    }
    /// Undefine a symbol's substitute value, returning the new descriptor if necessary
    pub fn removed(&self, symbol: &SymbolId) -> Option<EvalDesc> {
        self.removed_in(symbol, &mut ())
    }
    /// Substitute a value for a symbol. Return a type error on failure, and whether a change was made on success.
    ///
    /// # Examples
    /// ```rust
    /// # use rain_ir::eval::EvalDesc;
    /// # use rain_ir::value::*;
    /// # use rain_ir::primitive::nats::*;
    /// # use rain_ir::typing::*;
    /// let zero = Natural::from(0u128).into_val();
    /// let nat_var = SymbolId::new(Nats.into_repr());
    /// let mut desc = EvalDesc::default();
    /// assert_eq!(desc.len(), 0);
    /// assert!(desc.insert(nat_var.clone(), zero).is_ok());
    /// assert_eq!(desc.len(), 1);
    /// let mut desc_clone = desc.clone();
    /// assert!(
    ///     desc_clone.insert(nat_var.clone(), TYPE.clone_as_val()).is_err()
    /// );
    /// assert_eq!(desc, desc_clone);
    /// ```
    pub fn insert(&mut self, symbol: SymbolId, value: ValId) -> Result<Match, Error> {
        self.insert_in(symbol, value, &mut ())
    }
    /// Undefine a symbol's substitute value, returning whether a change was made
    pub fn remove(&mut self, symbol: &SymbolId) -> bool {
        self.remove_in(symbol, &mut ())
    }

    /// Substitute a value for a symbol. Return a type error on failure, and a new descriptor (if necessary) on success.
    pub fn inserted_in<C>(
        &self,
        symbol: SymbolId,
        value: ValId,
        ctx: &mut C,
    ) -> Result<(Match, Option<EvalDesc>), Error>
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        let match_ = symbol.repr().match_repr(value.repr().as_val())?;
        let universe_delta = self.universe_delta.max(match_.universe_delta());
        let result = self
            .symbols
            .inserted_in(symbol, value, ctx)
            .map(|symbols| EvalDesc {
                symbols,
                universe_delta,
                ..*self
            });
        Ok((match_, result))
    }
    /// Undefine a symbol's substitute value, returning the new descriptor if necessary
    pub fn removed_in<C>(&self, symbol: &SymbolId, ctx: &mut C) -> Option<EvalDesc>
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        self.symbols
            .removed_in(symbol, ctx)
            .map(|symbols| EvalDesc { symbols, ..*self })
    }
    /// Substitute a value for a symbol. Return a type error on failure, and whether a change was made on success.
    pub fn insert_in<C>(
        &mut self,
        symbol: SymbolId,
        value: ValId,
        ctx: &mut C,
    ) -> Result<Match, Error>
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        let match_ = symbol.repr().match_repr(value.repr().as_val())?;
        self.set_min_universe_delta(match_.universe_delta());
        self.symbols.insert_in(symbol, value, ctx);
        Ok(match_)
    }
    /// Undefine a symbol's substitute value, returning whether a change was made
    pub fn remove_in<C>(&mut self, symbol: &SymbolId, ctx: &mut C) -> bool
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        self.symbols.remove_in(symbol, ctx)
    }
    /// Whether this evaluation should be normalizing
    pub fn normalize(&self) -> bool {
        self.normalize
    }
    /// Get the substitution symbol set
    pub fn symbols(&self) -> &IdMap<SymbolId, ValId> {
        &self.symbols
    }
    /// Get the number of symbols defined in this descriptor
    #[inline]
    pub fn len(&self) -> usize {
        self.symbols.len()
    }
    /// Get whether this descriptor is empty
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.symbols.is_empty()
    }
    /// Get the universe delta of this descriptor
    #[inline]
    pub fn universe_delta(&self) -> u64 {
        self.universe_delta
    }
    /// Set the universe delta above the given minimum
    ///
    /// Return whether a change occured.
    ///
    /// # Examples
    /// ```rust
    /// # use rain_ir::eval::*;
    /// let mut desc = EvalDesc::default();
    /// assert_eq!(desc.universe_delta(), 0);
    /// desc.set_min_universe_delta(2);
    /// assert_eq!(desc.universe_delta(), 2);
    /// desc.set_min_universe_delta(1);
    /// assert_eq!(desc.universe_delta(), 2);
    /// ```
    #[inline]
    pub fn set_min_universe_delta(&mut self, delta: u64) -> bool {
        let universe_delta = self.universe_delta.max(delta);
        let change = self.universe_delta != universe_delta;
        self.universe_delta = universe_delta;
        change
    }
    /// Set whether this constant should be normalizing
    ///
    /// Return whether a change occured
    #[inline]
    pub fn set_normalize(&mut self, normalize: bool) -> bool {
        let change = self.normalize != normalize;
        self.normalize = normalize;
        change
    }
}

/// An evaluation context
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct EvalCtx {
    /// The descriptor for this context
    desc: EvalDesc,
    /// The evaluation cache
    cache: FxHashMap<ValByAddr, ValId>,
}

impl EvalCtx {
    /// Substitute a value for a symbol. Return a type error on failure, and a new descriptor (if necessary) on success.
    pub fn inserted(
        &self,
        symbol: SymbolId,
        value: ValId,
    ) -> Result<(Match, Option<EvalCtx>), Error> {
        self.inserted_in(symbol, value, &mut ())
    }
    /// Undefine a symbol's substitute value, returning the new descriptor if necessary
    pub fn removed(&self, symbol: &SymbolId) -> Option<EvalCtx> {
        self.removed_in(symbol, &mut ())
    }
    /// Substitute a value for a symbol. Return a type error on failure, and whether a change was made on success.
    pub fn insert(&mut self, symbol: SymbolId, value: ValId) -> Result<Match, Error> {
        self.insert_in(symbol, value, &mut ())
    }
    /// Undefine a symbol's substitute value, returning whether a change was made
    pub fn remove(&mut self, symbol: &SymbolId) -> bool {
        self.remove_in(symbol, &mut ())
    }

    /// Substitute a value for a symbol. Return a type error on failure, and a new descriptor (if necessary) on success.
    pub fn inserted_in<C>(
        &self,
        symbol: SymbolId,
        value: ValId,
        ctx: &mut C,
    ) -> Result<(Match, Option<EvalCtx>), Error>
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        let (match_, desc) = self.desc.inserted_in(symbol, value, ctx)?;
        Ok((match_, desc.map(EvalCtx::from)))
    }
    /// Undefine a symbol's substitute value, returning the new descriptor if necessary
    pub fn removed_in<C>(&self, symbol: &SymbolId, ctx: &mut C) -> Option<EvalCtx>
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        self.desc.removed_in(symbol, ctx).map(|desc| EvalCtx {
            desc,
            ..EvalCtx::default()
        })
    }
    /// Substitute a value for a symbol. Return a type error on failure, and whether a change was made on success.
    pub fn insert_in<C>(
        &mut self,
        symbol: SymbolId,
        value: ValId,
        ctx: &mut C,
    ) -> Result<Match, Error>
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        let result = self.desc.insert_in(symbol, value, ctx)?;
        self.cache.clear();
        Ok(result)
    }
    /// Undefine a symbol's substitute value, returning whether a change was made
    pub fn remove_in<C>(&mut self, symbol: &SymbolId, ctx: &mut C) -> bool
    where
        C: ConsCtx<SymbolId, ValId>,
    {
        let result = self.desc.remove_in(symbol, ctx);
        if result {
            self.cache.clear();
        }
        result
    }
    /// Lookup a value's cached evaluation, if any
    pub fn eval_cached(&self, value: &ValId) -> Option<&ValId> {
        self.cache.get(value.as_addr())
    }
    /// Get this context's descriptor
    pub fn desc(&self) -> &EvalDesc {
        &self.desc
    }
    /// Clone this context's descriptor
    pub fn clone_desc(&self) -> EvalDesc {
        self.desc.clone()
    }
    /// Evaluate a value
    pub fn eval(&mut self, value: &ValId) -> Option<ValId> {
        self.eval_cached(&value).cloned().or_else(|| {
            let result = value.substitute(self);
            if let Some(result) = &result {
                if self.should_cache {
                    self.cache.insert(value.clone_as_addr(), result.clone());
                }
            }
            result
        })
    }
    /// Set the universe delta above the given minimum
    /// 
    /// Return whether a change occured
    ///
    /// # Examples
    /// ```rust
    /// # use rain_ir::eval::*;
    /// let mut desc = EvalCtx::default();
    /// assert_eq!(desc.universe_delta(), 0);
    /// desc.set_min_universe_delta(2);
    /// assert_eq!(desc.universe_delta(), 2);
    /// desc.set_min_universe_delta(1);
    /// assert_eq!(desc.universe_delta(), 2);
    /// ```
    #[inline]
    pub fn set_min_universe_delta(&mut self, delta: u64) -> bool {
        let change = self.desc.set_min_universe_delta(delta);
        if change { self.cache.clear() }
        change
    }
    /// Set whether this constant should be normalizing
    ///
    /// Return whether a change occured
    #[inline]
    pub fn set_normalize(&mut self, normalize: bool) -> bool {
        let change = self.desc.set_normalize(normalize);
        if change { self.cache.clear() }
        change
    }
}

impl Deref for EvalCtx {
    type Target = EvalDesc;
    #[inline]
    fn deref(&self) -> &EvalDesc {
        &self.desc
    }
}

impl From<EvalDesc> for EvalCtx {
    #[inline]
    fn from(desc: EvalDesc) -> EvalCtx {
        EvalCtx {
            desc,
            ..EvalCtx::default()
        }
    }
}

impl From<EvalCtx> for EvalDesc {
    #[inline]
    fn from(ctx: EvalCtx) -> EvalDesc {
        ctx.desc
    }
}

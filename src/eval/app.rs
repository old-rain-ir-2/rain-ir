/*!
Function application
*/
use super::*;

/// An S-expression
#[derive(Debug, Clone, Eq)]
pub struct Sexpr {
    /// The function being applied
    func: ValId,
    /// The value it is being applied to
    arg: ValId,
    /// The representation of this S-expression
    repr: ReprId,
    /// The free variable set of this S-expression
    fv: SymbolSet,
    /// The constant variable set of this S-expression
    cv: SymbolSet,
}

impl Sexpr {
    /// Construct a new S-expression from an application. Return an error on invalid argument types
    pub fn new(func: ValId, arg: ValId) -> Result<Sexpr, Error> {
        Self::new_in(func, arg, &mut ())
    }
    /// Construct a new S-expression from an application. Return an error on invalid argument types
    pub fn new_in<C>(func: ValId, arg: ValId, ctx: &mut C) -> Result<Sexpr, Error>
    where
        C: ConsCtx<SymbolId, ()>,
    {
        let repr = func
            .repr()
            .apply_repr(ArgStack::unary(arg.clone()), &mut EvalCtx::default())?
            .expect("A function applied should never return it's own type!");
        let fv = func.fv().unioned_in(arg.fv(), ctx);
        let cv = if repr.is_ty() {
            SymbolSet::EMPTY
        } else {
            //TODO: fixed arguments
            func.cv().unioned_in(arg.cv(), ctx)
        };
        Ok(Sexpr {
            func,
            arg,
            repr,
            fv,
            cv,
        })
    }
    /// Construct an S-expression from an argument and a tail of (optional) additional arguments
    pub fn unfold_in<C, I>(func: ValId, arg: ValId, args: I, ctx: &mut C) -> Result<Sexpr, Error>
    where
        C: ConsCtx<SymbolId, ()>,
        I: Iterator,
        I::Item: Borrow<ValId>,
    {
        let mut result = Sexpr::new_in(func, arg, ctx)?;
        for arg in args {
            result = Sexpr::new_in(result.into_val(), arg.borrow().clone(), ctx)?;
        }
        Ok(result)
    }
}

impl PartialEq for Sexpr {
    #[inline]
    fn eq(&self, other: &Sexpr) -> bool {
        self.func.eq(&other.func) && self.arg.eq(&other.arg)
    }
}

impl Hash for Sexpr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.func.hash(state);
        self.arg.hash(state);
    }
}

impl MaybeType for Sexpr {
    #[inline]
    fn is_ty(&self) -> bool {
        self.repr().is_kind()
    }
}

impl MaybeRepr for Sexpr {
    #[inline]
    fn is_repr(&self) -> bool {
        self.repr().is_kind()
    }
}

impl MaybeReg for Sexpr {
    #[inline]
    fn is_reg(&self) -> bool {
        self.repr() == REGISTERS.as_repr()
    }
}

impl Typed for Sexpr {
    fn repr(&self) -> &ReprId {
        &self.repr
    }
}

impl Value for Sexpr {
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Sexpr(self)
    }
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    fn apply(&self, mut arg_stack: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ValId>, Error> {
        if arg_stack.is_empty() {
            return Ok(self.substitute(ctx));
        }
        //TODO: deal with irreducible functions
        arg_stack.push(self.arg.clone());
        self.func.apply(arg_stack, ctx)
    }
}

/*!
Booleans and logical operations
*/
use super::*;
use nats::*;
use scalar::*;

/// The default boolean representation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Bool;

impl Typed for Bool {
    #[inline]
    fn repr(&self) -> &ReprId {
        TYPE_REPR.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        TYPE_REPR.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl MaybeType for Bool {
    #[inline(always)]
    fn is_ty(&self) -> bool {
        true
    }
}

impl MaybeRepr for Bool {
    #[inline(always)]
    fn is_repr(&self) -> bool {
        true
    }
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        unimplemented!("{:?} <= {:?}", self, other)
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(Some(SMALL_FINITE[2].as_ty()))
    }
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Ok(Some(BOOL_REG.as_reg()))
    }
}

impl MaybeReg for Bool {}

impl Value for Bool {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Bool(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        BOOL.clone_as_val()
    }
}

impl Repr for Bool {}

impl Typed for bool {
    #[inline]
    fn repr(&self) -> &ReprId {
        BOOL.as_repr()
    }
}

impl MaybeRepr for bool {}

impl MaybeType for bool {}

impl MaybeReg for bool {}

impl Value for bool {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Boolean(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        let result = if self { &*TRUE } else { &*FALSE };
        result.clone_as_val()
    }
}

lazy_static! {
    /// The default boolean representation
    pub static ref BOOL: VarId<Bool> = VarId::from_var_direct(Bool);
    /// The boolean constant `true`
    pub static ref TRUE: VarId<bool> = VarId::from_var_direct(true);
    /// The boolean constant `false`
    pub static ref FALSE: VarId<bool> = VarId::from_var_direct(false);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn boolean_sanity_check() {
        let b = &*BOOL;
        let t = &*TRUE;
        let f = &*FALSE;
        let bin = &SMALL_FINITE[2];
        let br = &*BOOL_REG;
        for v in [t, f].iter().copied() {
            assert_eq!(v.ty(), bin);
            assert_eq!(v.repr(), b);
            assert_eq!(v.reg().unwrap(), br);
        }
    }
}

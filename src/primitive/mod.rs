/*!
Primitive `rain` types
*/
use crate::error::*;
use crate::eval::*;
use crate::function::*;
use crate::repr::*;
use crate::typing::*;
use crate::value::*;
use lazy_static::lazy_static;

pub mod logical;
pub mod nats;
pub mod scalar;
use nats::*;

/// The unit representation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Unit;

/// The inhabitant of the unit type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Nil;

/// The empty representation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Empty;

/// A zero-sized register representing a propositional type.
/// Every register may be freely cast to this one.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Zst;

/// A zero-sized register representing an *empty* type.
/// Can be freely cast to every other register.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Void;

impl MaybeRepr for Unit {
    #[inline]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline]
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        unimplemented!("{:?} <= {:?}", self, other)
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(Some(SMALL_FINITE[1].as_ty()))
    }
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Ok(Some(ZST.as_reg()))
    }
}

impl MaybeType for Unit {}

impl MaybeReg for Unit {}

impl Typed for Unit {
    #[inline]
    fn repr(&self) -> &ReprId {
        TYPE_REPR.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        TYPE_REPR.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Value for Unit {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Unit(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        UNIT.clone_as_val()
    }
}

impl Repr for Unit {}

impl MaybeRepr for Empty {
    #[inline]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline]
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        unimplemented!("{:?} <= {:?}", self, other)
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(Some(SMALL_FINITE[0].as_ty()))
    }
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Ok(Some(VOID.as_reg()))
    }
}

impl MaybeType for Empty {}

impl MaybeReg for Empty {}

impl Typed for Empty {
    #[inline]
    fn repr(&self) -> &ReprId {
        TYPE_REPR.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        TYPE_REPR.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Value for Empty {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Empty(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        EMPTY.clone_as_val()
    }
}

impl Repr for Empty {}

impl MaybeRepr for Zst {}

impl MaybeType for Zst {}

impl MaybeReg for Zst {
    fn is_reg(&self) -> bool {
        true
    }
}

impl Typed for Zst {
    #[inline]
    fn repr(&self) -> &ReprId {
        REGISTERS.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        REGISTERS.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Value for Zst {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Zst(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        ZST.clone_as_val()
    }
}

impl Register for Zst {}

impl MaybeRepr for Void {}

impl MaybeType for Void {}

impl MaybeReg for Void {
    fn is_reg(&self) -> bool {
        true
    }
}

impl Typed for Void {
    #[inline]
    fn repr(&self) -> &ReprId {
        REGISTERS.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        REGISTERS.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Value for Void {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Void(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        VOID.clone_as_val()
    }
}

impl Register for Void {}

lazy_static! {
    /// The unit representation
    pub static ref UNIT: VarId<Unit> = VarId::from_var_direct(Unit);
    /// The empty representation
    pub static ref EMPTY: VarId<Empty> = VarId::from_var_direct(Empty);
    /// The zero-sized register
    pub static ref ZST: VarId<Zst> = VarId::from_var_direct(Zst);
    /// The empty register
    pub static ref VOID: VarId<Void> = VarId::from_var_direct(Void);
}

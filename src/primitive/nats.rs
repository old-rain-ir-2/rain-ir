/*!
The natural numbers type and associated utilities
*/
use super::*;
use num::{BigUint, ToPrimitive};

/// The natural numbers type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Nats;

impl MaybeReg for Nats {}

impl MaybeRepr for Nats {
    #[inline(always)]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline]
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        self.match_ty(other.try_repr_ty()?.map(TypeId::as_val).unwrap_or(other))
            .map(Match::Type)
    }
    #[inline(always)]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(None)
    }
    #[inline(always)]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Ok(None)
    }
}

impl Type for Nats {}

impl MaybeType for Nats {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn match_ty(&self, other: &ValId) -> Result<TypeMatch, Error> {
        match other.as_enum() {
            ValueEnum::Nats(_) => Ok(TypeMatch::default()),
            _ => {
                if let Some(other) = other.normal_form() {
                    self.match_ty(&other)
                } else {
                    Err(Error::TypeMismatch(
                        "Nats is not a subtype of any other type",
                    ))
                }
            }
        }
    }
}

impl Typed for Nats {
    #[inline]
    fn repr(&self) -> &ReprId {
        TYPE.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        TYPE.as_ty()
    }
}

impl Value for Nats {
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Nats(self)
    }
    fn into_val(self) -> ValId {
        NATS.clone_as_val()
    }
}

/// A natural number literal
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Natural {
    /// The underlying data of this literal
    data: BigUint,
    /// The representation of this literal
    repr: ReprId,
}

impl Natural {
    /// Construct a new natural number literal
    pub fn new(data: BigUint) -> Natural {
        Natural {
            data,
            repr: NATS.clone_as_repr(),
        }
    }
}

impl From<u128> for Natural {
    #[inline]
    fn from(value: u128) -> Natural {
        Natural::new(value.into())
    }
}

impl From<usize> for Natural {
    #[inline]
    fn from(value: usize) -> Natural {
        Natural::new(value.into())
    }
}

impl MaybeReg for Natural {}

impl MaybeRepr for Natural {}

impl MaybeType for Natural {}

impl Typed for Natural {
    #[inline]
    fn repr(&self) -> &ReprId {
        &self.repr
    }
}

impl Value for Natural {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Natural(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        if self.repr == *NATS {
            if let Some(small) = self.data.to_u8() {
                if small < 16 {
                    return SMALL_NATS[small as usize].clone_as_val();
                }
            }
        }
        ValId::new_direct(self.into_enum())
    }
}

/// The type family of finite valued types
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Finite;

impl MaybeReg for Finite {}

impl MaybeRepr for Finite {}

impl MaybeType for Finite {}

impl Typed for Finite {
    fn repr(&self) -> &ReprId {
        NATS_IX_FIN.as_repr()
    }
    fn ty(&self) -> &TypeId {
        NATS_IX_FIN.as_ty()
    }
}

impl Value for Finite {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Finite(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        FINITE.clone_as_val()
    }
}

lazy_static! {
    /// The type of natural numbers
    pub static ref NATS: VarId<Nats> = VarId::from_var_direct(Nats);
    /// The type of families of finite types indexed by the natural numbers
    pub static ref NATS_IX_FIN: VarId<Pi> = VarId::from_var(
        Pi::function(NATS.clone_as_ty(), TYPE.clone_as_ty())
    );
    /// The type family of finite types
    pub static ref FINITE: VarId<Finite> = VarId::from_var_direct(Finite);
    /// The natural numbers from 0 to 16
    pub static ref SMALL_NATS: [VarId<Natural>; 16] = [
        VarId::from_var_direct(Natural::new(0u32.into())),
        VarId::from_var_direct(Natural::new(1u32.into())),
        VarId::from_var_direct(Natural::new(2u32.into())),
        VarId::from_var_direct(Natural::new(3u32.into())),
        VarId::from_var_direct(Natural::new(4u32.into())),
        VarId::from_var_direct(Natural::new(5u32.into())),
        VarId::from_var_direct(Natural::new(6u32.into())),
        VarId::from_var_direct(Natural::new(7u32.into())),
        VarId::from_var_direct(Natural::new(8u32.into())),
        VarId::from_var_direct(Natural::new(9u32.into())),
        VarId::from_var_direct(Natural::new(10u32.into())),
        VarId::from_var_direct(Natural::new(11u32.into())),
        VarId::from_var_direct(Natural::new(12u32.into())),
        VarId::from_var_direct(Natural::new(13u32.into())),
        VarId::from_var_direct(Natural::new(14u32.into())),
        VarId::from_var_direct(Natural::new(15u32.into())),
    ];
    /// The small finite types
    pub static ref SMALL_FINITE: [TypeId; 16] = {
        let small_nats = &*SMALL_NATS;
        [
            Sexpr::new(FINITE.clone_as_val(), small_nats[0].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[1].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[2].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[3].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[4].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[5].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[6].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[7].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[8].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[9].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[10].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[11].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[12].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[13].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[14].clone_as_val())
            .unwrap().into_ty(),
            Sexpr::new(FINITE.clone_as_val(), small_nats[15].clone_as_val())
            .unwrap().into_ty(),
        ]
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn small_natural_sanity_check() {
        let small_nats = &*SMALL_NATS;
        let nats = &*NATS;
        for (i, nat) in small_nats.iter().enumerate() {
            assert_eq!(nat.repr(), nats);
            assert_eq!(nat.ty(), nats);
            assert_eq!(nat.reg(), None);
            assert_eq!(*nat.fv(), SymbolSet::EMPTY);
            assert_eq!(nat.normal_form(), None);
            assert_eq!(*nat, Natural::from(i).into_val());
        }
    }

    #[test]
    fn finite_sanity_check() {
        let small_nats = &*SMALL_NATS;
        let finite = &*FINITE;
        let nats_ix_fin = &*NATS_IX_FIN;
        let fin = &*TYPE;
        let zero = &small_nats[0];
        assert_eq!(*finite.ty(), *nats_ix_fin);
        assert_eq!(
            nats_ix_fin
                .apply_repr(ArgStack::unary(zero.clone_as_val()), &mut EvalCtx::default())
                .unwrap()
                .unwrap(),
            *fin
        );
    }

    #[test]
    fn small_finite_sanity_check() {
        let small_nats = &*SMALL_NATS;
        let fin = &*TYPE;
        let finite = &*FINITE;
        let small_finite = &*SMALL_FINITE;
        for (nat, fin_ty) in small_nats.iter().zip(small_finite) {
            assert_eq!(*fin_ty.ty(), *fin);
            let fin_app = Sexpr::new(finite.clone_as_val(), nat.clone_as_val())
                .unwrap()
                .into_val();
            assert_eq!(fin_app, *fin_ty);
            assert_eq!(fin_ty.ty(), &*TYPE);
        }
    }
}

/*!
Scalar `rain` representations and registers
*/
use super::*;

/// Scalar `rain` registers
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum ScalarReg {
    /// Boolean scalar registers
    Bool,
    /// byte-size scalar registers
    U8,
    /// short-size scalar registers
    U16,
    /// int-size scalar registers
    U32,
    /// long-size scalar registers
    U64,
}

impl ScalarReg {
    /// The values of this enumeration as an array
    pub const VALUES: [ScalarReg; 5] = [
        ScalarReg::Bool,
        ScalarReg::U8,
        ScalarReg::U16,
        ScalarReg::U32,
        ScalarReg::U64,
    ];
}

impl MaybeReg for ScalarReg {
    #[inline(always)]
    fn is_reg(&self) -> bool {
        true
    }
}

impl MaybeRepr for ScalarReg {}

impl MaybeType for ScalarReg {}

impl Typed for ScalarReg {
    fn repr(&self) -> &ReprId {
        REGISTERS.as_repr()
    }
    fn ty(&self) -> &TypeId {
        REGISTERS.as_ty()
    }
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Register for ScalarReg {}

impl Value for ScalarReg {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::ScalarReg(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        use ScalarReg::*;
        let reg = match self {
            Bool => &*BOOL_REG,
            U8 => &*U8_REG,
            U16 => &*U16_REG,
            U32 => &*U32_REG,
            U64 => &*U64_REG,
        };
        reg.clone_as_val()
    }
}

lazy_static! {
    /// A boolean register
    pub static ref BOOL_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg::Bool);
    /// A byte register
    pub static ref U8_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg::U8);
    /// A U16 register
    pub static ref U16_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg::U16);
    /// A U32 register
    pub static ref U32_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg::U32);
    /// A U64 register
    pub static ref U64_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg::U64);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scalar_register_sanity_test() {
        let registers = &*REGISTERS;
        let scalar_values = [&*BOOL_REG, &*U8_REG, &*U16_REG, &*U32_REG, &*U64_REG];
        for (value, valid) in ScalarReg::VALUES.iter().zip(scalar_values.iter().copied()) {
            assert_eq!(value.ty(), registers);
            assert_eq!(valid.ty(), registers);
            assert!(value.is_reg());
            assert!(valid.is_reg());
            assert_eq!(value.into_val(), *valid);
            assert_eq!(value.into_enum(), *valid.as_enum());
        }
    }
}

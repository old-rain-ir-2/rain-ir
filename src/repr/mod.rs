/*!
`rain` machine representations and registers
*/
use crate::error::*;
use crate::eval::EvalCtx;
use crate::typing::*;
use crate::value::*;
use lazy_static::lazy_static;
use std::convert::Infallible;
use std::ops::BitAnd;

/// A trait implemented by all `rain` representations
pub trait Repr: Value {}

/// A trait implemented by all `rain` values which may be a representation
pub trait MaybeRepr: Clone {
    /// Whether this value is a representation
    #[inline(always)]
    fn is_repr(&self) -> bool {
        false
    }
    /// Attempt to match this representation for a substitution
    #[inline(always)]
    fn match_repr(&self, _other: &ValId) -> Result<Match, Error> {
        Err(Error::NotARepresentation("match_repr"))
    }
    /// If this is a representation, return the type it represents (or `None` if self)
    #[inline(always)]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Err(Error::NotARepresentation("try_repr_ty"))
    }
    /// If this is a representation, return the register it interprets if any
    #[inline(always)]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Err(Error::NotARepresentation("try_repr_reg"))
    }
    /// Attempt to apply this value to a set of arguments as a function representation
    fn apply_repr(&self, arg_stack: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ReprId>, Error>
    where
        Self: Value,
    {
        if arg_stack.is_empty() {
            if let Some(this) = self.substitute(ctx) {
                this.try_into_repr().map(Some)
            } else if self.is_repr() {
                Ok(None)
            } else {
                Err(Error::NotAFunctionRepr("apply_repr (empty)"))
            }
        } else {
            Err(Error::NotAFunctionRepr("apply_repr (full)"))
        }
    }
    /// Get the type this representation represents, or `None` if self
    fn repr_ty(&self) -> Option<&TypeId> {
        self.try_repr_ty().unwrap()
    }
    /// Get the register this representation interprets, if any
    fn repr_reg(&self) -> Option<&RegId> {
        self.try_repr_reg().unwrap()
    }
}

/// A match
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Match {
    /// A pure type match
    Type(TypeMatch),
    /// A representation match
    Repr(ReprMatch),
}

impl Match {
    /// Whether this is a pure type match
    pub fn is_pure(&self) -> bool {
        matches!(self, Match::Type(_))
    }
    /// Whether this is a representation match
    pub fn is_repr(&self) -> bool {
        matches!(self, Match::Repr(_))
    }
    /// Get the universe delta of this match
    pub fn universe_delta(&self) -> u64 {
        match self {
            Match::Type(t) => t.universe_delta,
            Match::Repr(r) => r.ty_match.universe_delta,
        }
    }
}

impl From<Match> for TypeMatch {
    fn from(match_: Match) -> TypeMatch {
        match match_ {
            Match::Type(t) => t,
            Match::Repr(r) => r.ty_match,
        }
    }
}

impl PartialEq<TypeMatch> for Match {
    fn eq(&self, other: &TypeMatch) -> bool {
        if let Match::Type(ty) = self {
            ty.eq(other)
        } else {
            false
        }
    }
}

impl PartialEq<ReprMatch> for Match {
    fn eq(&self, other: &ReprMatch) -> bool {
        if let Match::Repr(repr) = self {
            repr.eq(other)
        } else {
            false
        }
    }
}

impl PartialEq<Match> for TypeMatch {
    fn eq(&self, other: &Match) -> bool {
        if let Match::Type(ty) = other {
            ty.eq(self)
        } else {
            false
        }
    }
}

impl PartialEq<Match> for ReprMatch {
    fn eq(&self, other: &Match) -> bool {
        if let Match::Repr(repr) = other {
            repr.eq(self)
        } else {
            false
        }
    }
}

impl BitAnd for Match {
    type Output = Match;
    fn bitand(self, other: Match) -> Match {
        use Match::*;
        match (self, other) {
            (Repr(left), Repr(right)) => Repr(left & right),
            (left, right) => Type(TypeMatch::from(left) & TypeMatch::from(right)),
        }
    }
}

/// A representation match
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct ReprMatch {
    /// The underlying type match
    pub ty_match: TypeMatch,
}

impl BitAnd for ReprMatch {
    type Output = ReprMatch;
    fn bitand(self, other: ReprMatch) -> ReprMatch {
        ReprMatch {
            ty_match: self.ty_match & other.ty_match,
        }
    }
}

impl From<ReprMatch> for TypeMatch {
    fn from(match_: ReprMatch) -> TypeMatch {
        match_.ty_match
    }
}

impl From<ReprMatch> for Match {
    fn from(match_: ReprMatch) -> Match {
        Match::Repr(match_)
    }
}

impl From<TypeMatch> for Match {
    fn from(match_: TypeMatch) -> Match {
        Match::Type(match_)
    }
}

/// The type of all `rain` registers
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Registers;

/// A trait implemented by all `rain` registers
pub trait Register: Value {}

/// A trait implemented by all `rain` values which may be a register
pub trait MaybeReg {
    /// Whether this value is a register
    #[inline(always)]
    fn is_reg(&self) -> bool {
        false
    }
}

lazy_static! {
    /// The type of all `rain` registers
    pub static ref REGISTERS: VarId<Registers> = VarId::from_var_direct(Registers);
}

impl MaybeRepr for Infallible {}

impl MaybeReg for Registers {}

impl MaybeType for Registers {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
}

impl MaybeRepr for Registers {
    #[inline]
    fn is_repr(&self) -> bool {
        true
    }
}

impl Typed for Registers {
    #[inline]
    fn repr(&self) -> &ReprId {
        TYPE.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        TYPE.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Type for Registers {}

impl Value for Registers {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Registers(self)
    }
    #[inline]
    fn into_val(self) -> ValId {
        REGISTERS.clone_as_val()
    }
}

impl Register for Infallible {}

impl MaybeReg for Infallible {}

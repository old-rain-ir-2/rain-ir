/*!
# rain-ir
The core of the `rain` intermediate representation, consisting of the in-memory `rain` DAG and methods for manipulating, normalizing, and type-checking it.
*/
#![forbid(missing_docs, missing_debug_implementations)]
#![warn(clippy::all)]

pub mod error;
pub mod eval;
pub mod function;
pub mod primitive;
pub mod repr;
pub mod typing;
pub mod value;

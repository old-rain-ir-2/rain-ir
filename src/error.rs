/*!
Simple error handling
*/

use crate::typing::*;
use failure::Fail;

/// A `rain` error
#[derive(Fail, Debug, Clone, Eq, PartialEq, Hash)]
pub enum Error {
    /// A value is not a representation
    #[fail(display = "Not a representation: {}", 0)]
    NotARepresentation(&'static str),
    /// A value is not a type
    #[fail(display = "Not a type: {}", 0)]
    NotAType(&'static str),
    /// A representation mismatch
    #[fail(display = "Representation mismatch: {}", 0)]
    RepresentationMismatch(&'static str),
    /// A type mismatch
    #[fail(display = "Type mismatch: {}", 0)]
    TypeMismatch(&'static str),
    /// Universe kind mismatch
    #[fail(display = "Universe kind mismatch: {:?}, {:?}", 0, 1)]
    UniverseKindMismatch(UniverseKind, UniverseKind),
    /// Not a function
    #[fail(display = "Not a function: {}", 0)]
    NotAFunction(&'static str),
    /// Not a function representation
    #[fail(display = "Not a function representation: {}", 0)]
    NotAFunctionRepr(&'static str),
    /// Not a register
    #[fail(display = "Not a register: {}", 0)]
    NotAReg(&'static str),
    /// A custom error message
    #[fail(display = "{}", 0)]
    Message(&'static str),
}

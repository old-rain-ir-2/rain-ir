/*!
Typing universes, and associated type-theoretic constructs.
*/
use super::*;
//use itertools::Itertools;
use crate::eval::*;
use lazy_static::lazy_static;
use once_cell::sync::OnceCell;
use std::hash::{Hash, Hasher};

/// A typing universe
///
/// Typing universes in `rain` are described as kinds, i.e. types of types, which are closed under pi-types and sigma-types.
/// Typing universes may further be categorized into "interpreted" and "uninterpreted" universes: a universe is interpreted
/// if it contains not just types, but also representations.
#[derive(Debug, Clone)]
pub struct Universe {
    kind: UniverseKind,
    level: u64,
    succ: OnceCell<VarId<Universe>>,
}

/// The kinds of typing universes
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum UniverseKind {
    /// A univalent universe of pure types
    Univalent = 0,
    /// A universe of unrestricted types
    Unrestricted = 1,
    /// A universe of affine types
    Affine = 2,
    /// A universe of linear types
    Linear = 3,
}

impl Universe {
    /// Create a new universe with the given level and kind
    pub fn new(level: u64, kind: UniverseKind) -> Universe {
        Universe {
            level,
            kind,
            succ: OnceCell::new(),
        }
    }
    /// Get the type of this universe, which is always another universe
    pub fn get_ty(&self) -> &VarId<Universe> {
        self.succ.get_or_init(|| {
            VarId::from_var(Universe {
                kind: self.kind,
                level: self.level + 1,
                succ: OnceCell::new(),
            })
        })
    }
    /// Get the kind of this universe
    pub fn kind(&self) -> UniverseKind {
        self.kind
    }
    /// Get the level of this universe
    pub fn level(&self) -> u64 {
        self.level
    }
}

impl PartialEq for Universe {
    fn eq(&self, other: &Universe) -> bool {
        self.kind == other.kind && self.level == other.level
    }
}

impl Eq for Universe {}

impl Hash for Universe {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.kind.hash(hasher);
        self.level.hash(hasher);
    }
}

impl MaybeRepr for Universe {
    #[inline]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(None)
    }
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Ok(None)
    }
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        self.match_ty(other.try_repr_ty()?.map(TypeId::as_val).unwrap_or(other))
            .map(Match::Type)
    }
    #[inline]
    fn repr_ty(&self) -> Option<&TypeId> {
        None
    }
    #[inline]
    fn repr_reg(&self) -> Option<&RegId> {
        None
    }
}

impl Type for Universe {}

impl MaybeType for Universe {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline(always)]
    fn is_kind(&self) -> bool {
        true
    }
    fn match_ty(&self, other: &ValId) -> Result<TypeMatch, Error> {
        match other.as_enum() {
            ValueEnum::Universe(other) => {
                if self.kind <= other.kind {
                    let universe_delta = self.level.saturating_sub(other.level);
                    Ok(TypeMatch { universe_delta })
                } else {
                    Err(Error::UniverseKindMismatch(self.kind, other.kind))
                }
            }
            _ => Err(Error::RepresentationMismatch(
                "Universes can only be sub-representations of other universes",
            )),
        }
    }
}

impl MaybeReg for Universe {}

impl Typed for Universe {
    fn repr(&self) -> &ReprId {
        self.get_ty().as_repr()
    }
    fn ty(&self) -> &TypeId {
        self.get_ty().as_ty()
    }
}

impl Value for Universe {
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Universe(self)
    }
    fn into_val(self) -> ValId {
        use UniverseKind::*;
        match (self.kind, self.level) {
            (Univalent, 0) => TYPE.clone_as_val(),
            (Unrestricted, 0) => TYPE_REPR.clone_as_val(),
            (Affine, 0) => TYPE_AFFINE.clone_as_val(),
            (Linear, 0) => TYPE_LINEAR.clone_as_val(),
            _ => ValId::new_direct(self.into_enum()),
        }
    }
    #[inline]
    fn substitute_norm(&self, ctx: &mut EvalCtx, _allow_norm: bool) -> Option<ValId> {
        let universe_delta = ctx.universe_delta();
        if universe_delta > 0 {
            Some(Universe::new(self.level + universe_delta, self.kind).into_val())
        } else {
            None
        }
    }
}

lazy_static! {
    /// The base polymorphic universe of types
    pub static ref TYPE: VarId<Universe> = VarId::from_var_direct(
        Universe {
            kind: UniverseKind::Univalent,
            level: 0,
            succ: OnceCell::new()
        }
    );
    /// The base polymorphic universe of interpreted types
    pub static ref TYPE_REPR: VarId<Universe> = VarId::from_var_direct(
        Universe {
            kind: UniverseKind::Unrestricted,
            level: 0,
            succ: OnceCell::new()
        }
    );
    /// The base polymorphic universe of interpreted types
    pub static ref TYPE_AFFINE: VarId<Universe> = VarId::from_var_direct(
        Universe {
            kind: UniverseKind::Affine,
            level: 0,
            succ: OnceCell::new()
        }
    );
    /// The base polymorphic universe of interpreted types
    pub static ref TYPE_LINEAR: VarId<Universe> = VarId::from_var_direct(
        Universe {
            kind: UniverseKind::Linear,
            level: 0,
            succ: OnceCell::new()
        }
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn universe_delta_substitution_works() {
        let ty = &*TYPE;
        let kind = ty.ty();
        let mut ctx = EvalCtx::default();
        assert_eq!(ty.substitute(&mut ctx), None);
        ctx.set_min_universe_delta(1);
        assert_eq!(ty.substitute(&mut ctx).unwrap(), *kind);
    }
}

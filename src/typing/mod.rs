/*!
The core of `rain`'s type system
*/
use crate::error::*;
use crate::repr::*;
use crate::value::*;
use std::convert::Infallible;
use std::ops::BitAnd;

mod universe;
pub use universe::*;

/// A trait implemented by all values with a `rain` type and representation
pub trait Typed {
    /// Get the representation of this value
    fn repr(&self) -> &ReprId;
    /// Get the type of this value
    #[inline]
    fn ty(&self) -> &TypeId {
        self.repr().get_repr_ty()
    }
    /// Get the register of this value, if any
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        self.repr().repr_reg()
    }
    /// Clone the representation of this value
    #[inline]
    fn clone_repr(&self) -> ReprId {
        self.repr().clone()
    }
    /// Clone the type of this value
    #[inline]
    fn clone_ty(&self) -> TypeId {
        self.ty().clone()
    }
    /// Clone the register of this value
    #[inline]
    fn clone_reg(&self) -> Option<RegId> {
        self.reg().cloned()
    }
}

/// A trait implemented by all `rain` types
pub trait Type: Value {}

impl<T: Type> Repr for T {}

/// A trait implemented by all `rain` values which may be a type
pub trait MaybeType: MaybeRepr {
    /// Check whether this value is a type
    #[inline(always)]
    fn is_ty(&self) -> bool {
        false
    }
    /// Check whether this value is a kind
    #[inline(always)]
    fn is_kind(&self) -> bool {
        false
    }
    /// Check whether this type matches another
    #[inline]
    fn match_ty(&self, _other: &ValId) -> Result<TypeMatch, Error> {
        Err(Error::NotAType("match_ty"))
    }
}

/// A type match
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct TypeMatch {
    /// The universe delta
    pub universe_delta: u64,
}

impl BitAnd for TypeMatch {
    type Output = TypeMatch;
    fn bitand(self, other: TypeMatch) -> TypeMatch {
        TypeMatch {
            universe_delta: self.universe_delta.max(other.universe_delta),
        }
    }
}

impl Typed for Infallible {
    fn repr(&self) -> &ReprId {
        match *self {}
    }
    fn ty(&self) -> &TypeId {
        match *self {}
    }
}

impl MaybeType for Infallible {}

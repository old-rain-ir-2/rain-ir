/*!
Predicates for `rain` values
*/
use super::*;

/// A trait implemented by all predicates
pub trait Pred {}

/// A trait implemented by all predicates which indicate a value is a type
pub trait TypePred: ReprPred {}

/// A trait implemented by all predicates which indicate a value is a representation
pub trait ReprPred: Pred {}

/// A trait implemented by all predicates which indicate a value is a register
pub trait RegPred: Pred {}

/// A predicate asserting a value is a type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct IsType;

impl Pred for IsType {}

impl TypePred for IsType {}

impl ReprPred for IsType {}

/// A predicate asserting a value is a representation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct IsRepr;

impl Pred for IsRepr {}

impl ReprPred for IsRepr {}

/// A predicate asserting a value is a register
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct IsReg;

impl Pred for IsReg {}

impl RegPred for IsReg {}

/// A predicate asserting a value is of a given type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Is<T: ?Sized>(PhantomData<T>);

impl<T: Value> Pred for Is<T> {}

impl<T: Type> TypePred for Is<T> {}

impl<T: Repr> ReprPred for Is<T> {}

impl<T: Register> RegPred for Is<T> {}

/*!
`rain` symbols
*/
use super::*;
use pour::{IdSet, RadixKey};

/// A symbol
#[derive(Debug, Clone, Eq)]
pub struct Symbol {
    /// The ID of this symbol
    id: SymbolId,
    /// The cached representation of this symbol
    repr: ReprId,
    /// The free variable set of this symbol, *plus* the symbol
    fv: SymbolSet,
}

impl Symbol {
    /// Create a new, unique `Symbol` with the given representation
    #[inline]
    pub fn new(repr: ReprId) -> Symbol {
        SymbolId::new(repr).into()
    }
    /// Get the `SymbolId` of this symbol
    #[inline]
    pub fn id(&self) -> &SymbolId {
        &self.id
    }
    /// Clone this `SymbolId`
    #[inline]
    pub fn clone_id(&self) -> SymbolId {
        self.id.clone()
    }
}

impl From<SymbolId> for Symbol {
    fn from(id: SymbolId) -> Symbol {
        let repr = id.repr().clone();
        let mut fv = repr.fv().clone();
        fv.insert(id.clone(), ());
        Symbol { id, repr, fv }
    }
}

impl From<Symbol> for SymbolId {
    fn from(symbol: Symbol) -> SymbolId {
        symbol.id
    }
}

impl PartialEq for Symbol {
    #[inline]
    fn eq(&self, other: &Symbol) -> bool {
        self.id == other.id
    }
}

impl Hash for Symbol {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.id.hash(hasher)
    }
}

/// A set of symbol IDs
pub type SymbolSet = IdSet<SymbolId>;

/// A symbol ID
#[derive(Debug, Clone)]
pub struct SymbolId {
    /// The inner implementation of this symbol
    inner: Arc<InnerSymbol>,
}

impl SymbolId {
    /// Create a new, unique symbol with a given representation
    #[inline]
    pub fn new(repr: ReprId) -> SymbolId {
        SymbolId {
            inner: Arc::new(InnerSymbol { repr }),
        }
    }
    /// Get the unique ID of this symbol
    #[inline]
    pub fn id(&self) -> usize {
        Arc::as_ptr(&self.inner) as usize
    }
}

impl Typed for SymbolId {
    #[inline]
    fn repr(&self) -> &ReprId {
        &self.inner.repr
    }
}

impl PartialEq for SymbolId {
    #[inline]
    fn eq(&self, other: &SymbolId) -> bool {
        Arc::ptr_eq(&self.inner, &other.inner)
    }
}

impl Eq for SymbolId {}

impl Hash for SymbolId {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        std::ptr::hash(&*self.inner, hasher)
    }
}

impl RadixKey for SymbolId {
    //TODO: usize
    type PatternType = usize;
    type DepthType = u8;
    #[inline(always)]
    fn pattern(&self, _depth: u8) -> usize {
        &*self.inner as *const _ as usize
    }
}

/// The inner implementation for a symbol
#[derive(Debug, Clone)]
struct InnerSymbol {
    /// The representation of this symbol
    repr: ReprId,
}

impl MaybeType for Symbol {
    #[inline]
    fn is_ty(&self) -> bool {
        self.repr().is_kind()
    }
}

impl MaybeRepr for Symbol {
    #[inline]
    fn is_repr(&self) -> bool {
        self.repr().is_kind()
    }
}

impl MaybeReg for Symbol {
    #[inline]
    fn is_reg(&self) -> bool {
        self.repr() == REGISTERS.as_repr()
    }
}

impl Typed for Symbol {
    fn repr(&self) -> &ReprId {
        &self.repr
    }
}

impl Value for Symbol {
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Symbol(self)
    }
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    fn substitute_norm(&self, ctx: &mut EvalCtx, normalize: bool) -> Option<ValId> {
        let mut sub = ctx.symbols().get(self.id()).cloned()?;
        if normalize {
            sub = sub.normal_form().unwrap_or_else(|| sub.clone());
        }
        Some(sub)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn symbol_substitution_test() {
        let id = SymbolId::new(NATS.clone_as_repr());
        let symbol = Symbol::from(id.clone()).into_val();
        assert_eq!(symbol.normal_form(), None);
        let mut ctx = EvalCtx::default();
        let small_nats = &*SMALL_NATS;
        let zero = &small_nats[0];
        let one = &small_nats[1];
        ctx.insert(id.clone(), zero.clone_as_val())
            .expect("0 is a natural number");
        assert_eq!(symbol.substitute(&mut ctx).unwrap(), *zero);
        ctx.insert(id.clone(), one.clone_as_val())
            .expect("1 is a natural number");
        assert_eq!(symbol.substitute(&mut ctx).unwrap(), *one);
        ctx.remove(&id);
        assert_eq!(symbol.substitute(&mut ctx), None);
    }
}

/*!
`ValId`s, the `ValueEnum`, and associated utilities.
*/
use crate::error::*;
use crate::eval::*;
use crate::function::*;
use crate::primitive::{logical::*, nats::*, scalar::*, *};
use crate::repr::*;
use crate::typing::*;
use elysees::Arc;
use once_cell::sync::OnceCell;
use ref_cast::RefCast;
use smallvec::smallvec;
use smallvec::SmallVec;
use std::convert::Infallible;
use std::fmt::{self, Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::iter::Rev;
use std::marker::PhantomData;
use std::ops::Deref;

mod symbol;
pub use symbol::*;
mod pred;
pub use pred::*;

/// A `rain` value
#[derive(Eq, RefCast)]
#[repr(transparent)]
pub struct ValId<P = ()> {
    ptr: Arc<Node>,
    pred: PhantomData<P>,
}

/// A `rain` type
pub type TypeId = ValId<IsType>;

/// A `rain` representation
pub type ReprId = ValId<IsRepr>;

/// A `rain` register
pub type RegId = ValId<IsReg>;

/// A `rain` value guaranteed to be a given variant
pub type VarId<V> = ValId<Is<V>>;

/// The `ValueEnum`, enumerating all `rain` values
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum ValueEnum {
    // === Basic components ===
    /// A symbol
    Symbol(Symbol),
    /// A lambda function
    Lambda(Lambda),
    /// A dependent function type
    Pi(Pi),
    /// An S-expression
    Sexpr(Sexpr),

    // === Type-theoretic primitives ===
    /// The unit representation
    Unit(Unit),
    /// The empty representation
    Empty(Empty),
    /// The propositional register
    Zst(Zst),
    /// The empty register
    Void(Void),

    // === Natural numbers ===
    /// The type of natural numbers
    Nats(Nats),
    /// The type family of finite-valued types
    Finite(Finite),
    /// A natural number literal
    Natural(Natural),

    // === Logic ===
    /// The standard boolean representation
    Bool(Bool),
    /// A boolean constant
    Boolean(bool),

    // === Typing universes ===
    /// A typing universe
    Universe(Universe),

    // === Representations and registers ===
    /// The type of all registers
    Registers(Registers),
    /// Scalar registers
    ScalarReg(ScalarReg),
    /// Static Function
    StaticFn(StaticFn),
}

/// The trait implemented by all `rain` values
pub trait Value: Clone + Sized + Typed + MaybeType + MaybeRepr + MaybeReg {
    /// Get this value as a member of the `ValueEnum`
    fn into_enum(self) -> ValueEnum;
    /// Get this value as a `ValId`
    #[inline]
    fn into_val(self) -> ValId {
        ValId::new_direct(self.into_enum())
    }
    /// Get the free value set of this value
    #[inline]
    fn fv(&self) -> &SymbolSet {
        &SymbolSet::EMPTY
    }
    /// Get the constant variable set of this value
    #[inline]
    fn cv(&self) -> &SymbolSet {
        if let Some(reg) = self.reg() {
            reg.fv()
        } else {
            &SymbolSet::EMPTY
        }
    }
    /// Try to get this value as a `ReprId`
    #[inline]
    fn try_into_repr(self) -> Result<ReprId, Error> {
        if self.is_repr() {
            Ok(self.into_val().coerce_unchecked())
        } else {
            Err(Error::NotARepresentation("try_into_repr"))
        }
    }
    /// Get this value as a `TypeId`
    #[inline]
    fn try_into_ty(self) -> Result<TypeId, Error> {
        if self.is_ty() {
            Ok(self.into_val().coerce_unchecked())
        } else {
            Err(Error::NotAType("try_into_ty"))
        }
    }
    /// Get this value as a `RegId`
    #[inline]
    fn try_into_reg(self) -> Result<RegId, Error> {
        if self.is_reg() {
            Ok(self.into_val().coerce_unchecked())
        } else {
            Err(Error::NotAReg("try_into_reg"))
        }
    }
    /// Get this value as a `ReprId`
    #[inline]
    fn into_repr(self) -> ReprId {
        self.try_into_repr().unwrap()
    }
    /// Get this value as a `ReprId`
    #[inline]
    fn into_ty(self) -> TypeId {
        self.try_into_ty().unwrap()
    }
    /// Get this value as a `ReprId`
    #[inline]
    fn into_reg(self) -> RegId {
        self.try_into_reg().unwrap()
    }
    /// Recursively substitute this value's components in a given context. Return `None` on no change.
    /// Normalizes dependent on context configuration.
    #[inline]
    fn substitute(&self, ctx: &mut EvalCtx) -> Option<ValId> {
        self.substitute_norm(ctx, ctx.normalize())
    }
    /// Recursively substitute this value's components in a given context. Return `None` on no change.
    /// Normalizes dependent on context configuration and whether normalization is allowed
    #[inline]
    fn substitute_norm(&self, _ctx: &mut EvalCtx, _normalize: bool) -> Option<ValId> {
        assert_eq!(
            *self.fv(),
            SymbolSet::EMPTY,
            "Default substitution implementation assumes empty symbol-set.",
        );
        None
    }
    /// Get this value, but normalized. Return `None` if no change.
    #[inline]
    fn normal_form(&self) -> Option<ValId> {
        None
    }
    /// Apply this value to a set of arguments within a given evaluation context
    #[inline]
    fn apply(&self, mut arg_stack: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ValId>, Error> {
        if let Some(arg) = arg_stack.pop() {
            Sexpr::unfold_in(
                self.clone().into_val(),
                arg,
                arg_stack.into_iter().rev(),
                &mut (),
            )
            .map(|expr| Some(expr.into_val()))
        } else {
            Ok(self.substitute(ctx))
        }
    }
}

/// An argument stack
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct ArgStack(pub SmallVec<[ValId; 2]>);

impl ArgStack {
    /// Create a new, empty stack
    pub fn new() -> ArgStack {
        ArgStack(SmallVec::new())
    }
    /// Create a new argument stack with one element
    pub fn unary(elem: ValId) -> ArgStack {
        ArgStack(smallvec![elem])
    }
    /// Create a new argument stack with two elements
    pub fn binary(left: ValId, right: ValId) -> ArgStack {
        ArgStack(smallvec![right, left])
    }
    /// Create a new argument stack with one element repeated `n` times
    pub fn repeat(elem: ValId, n: usize) -> ArgStack {
        ArgStack(SmallVec::from_elem(elem, n))
    }
    /// Pop the top element from this argument stack, returning it (if any)
    pub fn pop(&mut self) -> Option<ValId> {
        self.0.pop()
    }
    /// Peek at the top element from this argument stack, if any
    pub fn peek(&self) -> Option<&ValId> {
        self.0.last()
    }
    /// Push an element onto this argument stack
    pub fn push(&mut self, elem: ValId) {
        self.0.push(elem)
    }
    /// Get whether this argument stack is empty
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

impl IntoIterator for ArgStack {
    type IntoIter = Rev<<SmallVec<[ValId; 2]> as IntoIterator>::IntoIter>;
    type Item = ValId;
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter().rev()
    }
}

/// A `rain` value by address
#[derive(Clone, Eq, Debug, RefCast)]
#[repr(transparent)]
pub struct ValByAddr(pub ValId);

// === Implementation ===

/// Perform the same operation on every member of the `ValueEnum`
#[macro_export]
macro_rules! forv {
    ($v:expr ; $i:ident => $e:expr) => {
        match $v {
            ValueEnum::Symbol($i) => $e,
            ValueEnum::Lambda($i) => $e,
            ValueEnum::Pi($i) => $e,
            ValueEnum::Sexpr($i) => $e,
            ValueEnum::StaticFn($i) => $e,

            ValueEnum::Unit($i) => $e,
            ValueEnum::Empty($i) => $e,
            ValueEnum::Zst($i) => $e,
            ValueEnum::Void($i) => $e,

            ValueEnum::Finite($i) => $e,
            ValueEnum::Nats($i) => $e,
            ValueEnum::Natural($i) => $e,

            ValueEnum::Bool($i) => $e,
            ValueEnum::Boolean($i) => $e,

            ValueEnum::Universe($i) => $e,

            ValueEnum::Registers($i) => $e,
            ValueEnum::ScalarReg($i) => $e,
        }
    };
}

impl MaybeType for ValueEnum {
    #[inline]
    fn is_ty(&self) -> bool {
        forv!(self; v => v.is_ty())
    }
    #[inline]
    fn is_kind(&self) -> bool {
        forv!(self; v => v.is_kind())
    }
}

impl MaybeReg for ValueEnum {
    #[inline]
    fn is_reg(&self) -> bool {
        forv!(self; v => v.is_reg())
    }
}

impl MaybeRepr for ValueEnum {
    #[inline]
    fn is_repr(&self) -> bool {
        forv!(self; v => v.is_repr())
    }
    #[inline(always)]
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        forv!(self; v => v.match_repr(other))
    }
    #[inline(always)]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        forv!(self; v => v.try_repr_ty())
    }
    #[inline(always)]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        forv!(self; v => v.try_repr_reg())
    }
    #[inline(always)]
    fn apply_repr(&self, arg_stack: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ReprId>, Error> {
        forv!(self; v => v.apply_repr(arg_stack, ctx))
    }
}

impl Typed for ValueEnum {
    #[inline]
    fn repr(&self) -> &ReprId {
        forv!(self; v => v.repr())
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        forv!(self; v => v.ty())
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        forv!(self; v => v.reg())
    }
}

impl Value for ValueEnum {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        self
    }
    #[inline]
    fn into_val(self) -> ValId {
        forv!(self; v => v.into_val())
    }
    fn fv(&self) -> &SymbolSet {
        forv!(self; v => v.fv())
    }
    #[inline]
    fn substitute(&self, ctx: &mut EvalCtx) -> Option<ValId> {
        forv!(self; v => v.substitute(ctx))
    }
    #[inline]
    fn substitute_norm(&self, ctx: &mut EvalCtx, normalize: bool) -> Option<ValId> {
        forv!(self; v => v.substitute_norm(ctx, normalize))
    }
    #[inline]
    fn normal_form(&self) -> Option<ValId> {
        forv!(self; v => v.normal_form())
    }
}

impl PartialEq for ValByAddr {
    #[inline]
    fn eq(&self, other: &ValByAddr) -> bool {
        self.0.as_ptr() == other.0.as_ptr()
    }
}

impl Hash for ValByAddr {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.0.as_ptr().hash(hasher)
    }
}

impl<P, Q> PartialEq<ValId<P>> for ValId<Q> {
    #[inline]
    fn eq(&self, other: &ValId<P>) -> bool {
        Arc::ptr_eq(&self.ptr, &other.ptr) || self.as_enum() == other.as_enum()
    }
}

impl<P> Hash for ValId<P> {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.as_enum().hash(hasher)
    }
}

impl<P> MaybeType for ValId<P> {
    fn is_ty(&self) -> bool {
        self.as_enum().is_ty()
    }
    fn is_kind(&self) -> bool {
        self.as_enum().is_kind()
    }
}

impl<P> MaybeRepr for ValId<P> {
    fn is_repr(&self) -> bool {
        self.as_enum().is_repr()
    }
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        self.as_enum().match_repr(other)
    }
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        self.as_enum().try_repr_ty()
    }
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        self.as_enum().try_repr_reg()
    }
    fn apply_repr(&self, arg_stack: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ReprId>, Error> {
        self.as_enum().apply_repr(arg_stack, ctx)
    }
}

impl<P> MaybeReg for ValId<P> {
    #[inline]
    fn is_reg(&self) -> bool {
        self.as_enum().is_reg()
    }
}

impl<P> Typed for ValId<P> {
    #[inline]
    fn repr(&self) -> &ReprId {
        self.as_enum().repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        self.as_enum().ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        self.as_enum().reg()
    }
}

impl<P: TypePred> ValId<P> {
    /// Get this value as a type
    #[inline]
    pub fn as_ty(&self) -> &TypeId {
        self.coerce_ref_unchecked()
    }
    /// Clone this value as a type
    #[inline]
    pub fn clone_as_ty(&self) -> TypeId {
        self.as_ty().clone()
    }
}

impl<P: ReprPred> ValId<P> {
    /// Get this value as a representation
    #[inline]
    pub fn as_repr(&self) -> &ReprId {
        self.coerce_ref_unchecked()
    }
    /// Clone this value as a representation
    #[inline]
    pub fn clone_as_repr(&self) -> ReprId {
        self.as_repr().clone()
    }
    /// Get the type this representation is for. This always succeeds.
    #[inline]
    pub fn get_repr_ty(&self) -> &TypeId {
        self.as_enum()
            .try_repr_ty()
            .unwrap()
            .unwrap_or_else(|| self.coerce_ref_unchecked())
    }
}

impl<P: RegPred> ValId<P> {
    /// Get this value as a register
    #[inline]
    pub fn as_reg(&self) -> &RegId {
        self.coerce_ref_unchecked()
    }
}

impl<P: RegPred> Register for ValId<P> {}

impl<P> Value for ValId<P> {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        self.as_enum().clone()
    }
    #[inline]
    fn into_val(self) -> ValId {
        self.coerce_unchecked()
    }
    fn fv(&self) -> &SymbolSet {
        self.as_enum().fv()
    }
    #[inline]
    fn substitute(&self, ctx: &mut EvalCtx) -> Option<ValId> {
        self.as_enum().substitute(ctx)
    }
    #[inline]
    fn substitute_norm(&self, ctx: &mut EvalCtx, allow_norm: bool) -> Option<ValId> {
        self.as_enum().substitute_norm(ctx, allow_norm)
    }
    #[inline]
    fn normal_form(&self) -> Option<ValId> {
        self.as_enum().normal_form()
    }
}

impl Value for Infallible {
    fn into_enum(self) -> ValueEnum {
        match self {}
    }
}

impl<P> ValId<P> {
    /// Get the `ValueEnum` underlying this value
    #[inline]
    pub fn as_enum(&self) -> &ValueEnum {
        &self.ptr.value
    }
    /// Get this `ValId` as a pointer
    #[inline]
    pub fn as_ptr(&self) -> *const ValueEnum {
        self.as_enum() as *const _
    }
    /// Get this `ValId`'s address
    #[inline]
    pub fn as_addr(&self) -> &ValByAddr {
        RefCast::ref_cast(self.as_val())
    }
    /// Access this `ValId`'s normal pointer
    #[inline]
    pub fn normal(&self) -> Option<&ValId> {
        self.ptr
            .normal
            .get_or_init(|| self.as_enum().normal_form())
            .as_ref()
    }
    /// Get this `ValId` as a normalized `ValId`
    #[inline]
    pub fn as_normal(&self) -> &ValId {
        self.normal().unwrap_or_else(|| self.as_val())
    }
    /// Get whether this `ValId` is a normalized `ValId`
    #[inline]
    pub fn is_normal(&self) -> bool {
        self.normal().is_some()
    }
    /// Clone this value as an address
    #[inline]
    pub fn clone_as_addr(&self) -> ValByAddr {
        ValByAddr(self.clone_as_val())
    }
    /// Check two `ValId`'s are pointer-equal
    #[inline]
    pub fn ptr_eq<Q>(&self, other: &ValId<Q>) -> bool {
        self.as_ptr() == other.as_ptr()
    }
    /// Get this value as a `ValId`
    #[inline]
    pub fn as_val(&self) -> &ValId {
        self.coerce_ref_unchecked()
    }
    /// Clone this value as a `ValId`
    #[inline]
    pub fn clone_as_val(&self) -> ValId {
        self.as_val().clone()
    }
    /// Cast a `ValId` to another kind *without* checking validity
    #[inline]
    pub fn coerce_unchecked<Q>(self) -> ValId<Q> {
        ValId {
            ptr: self.ptr,
            pred: PhantomData,
        }
    }
    /// Cast a reference to a `ValId` to another kind *without* checking validity
    #[inline]
    pub fn coerce_ref_unchecked<Q>(&self) -> &ValId<Q> {
        RefCast::ref_cast(&self.ptr)
    }
    /// Cast a mutable reference to a `ValId` to another kind *without* checking validity
    #[inline]
    pub fn coerce_mut_unchecked<Q>(&mut self) -> &mut ValId<Q> {
        RefCast::ref_cast_mut(&mut self.ptr)
    }
}

impl AsMut<ValueEnum> for ValId {
    #[inline]
    fn as_mut(&mut self) -> &mut ValueEnum {
        &mut Arc::make_mut(&mut self.ptr).value
    }
}

impl<V: Value> VarId<V> {
    /// Create a new `VarId` from a value
    pub fn from_var(value: V) -> VarId<V> {
        value.into_val().coerce_unchecked()
    }
    /// Create a new `VarId` directly from a value
    pub fn from_var_direct(value: V) -> VarId<V> {
        ValId::new_direct(value.into_enum()).coerce_unchecked()
    }
}

impl ValId {
    /// Create a `ValId` directly from a `ValueEnum`
    pub fn new_direct(value: ValueEnum) -> ValId {
        ValId {
            ptr: Node::new(value).into(),
            pred: PhantomData,
        }
    }
}

impl<P> Clone for ValId<P> {
    #[inline]
    fn clone(&self) -> ValId<P> {
        ValId {
            ptr: self.ptr.clone(),
            pred: self.pred,
        }
    }
}

impl<P> Debug for ValId<P> {
    #[inline]
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Debug::fmt(&self.ptr, fmt)
    }
}

/// A node in the `rain` graph
#[derive(Debug, Clone)]
pub struct Node {
    /// The value of this node
    value: ValueEnum,
    /// The normal form of this node, if not `self`
    normal: OnceCell<Option<ValId>>,
}

impl Node {
    /// Create a new, empty node for a given value
    fn new(value: ValueEnum) -> Node {
        Node {
            value,
            normal: OnceCell::new(),
        }
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        self.value == other.value
    }
}

impl Eq for Node {}

impl Hash for Node {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.value.hash(hasher)
    }
}

impl Deref for ValId {
    type Target = ValueEnum;
    #[inline]
    fn deref(&self) -> &ValueEnum {
        self.as_enum()
    }
}

/*!
`rain` functions and function types
*/
use crate::error::*;
use crate::eval::*;
use crate::repr::*;
use crate::typing::*;
use crate::value::*;
use pour::*;
use std::hash::{Hash, Hasher};

mod lambda;
mod pi;
mod static_fn;
pub use lambda::*;
pub use pi::*;
pub use static_fn::*;
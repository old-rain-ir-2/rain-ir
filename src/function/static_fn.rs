/*!
A simple static top-level function type.
*/
use super::*;

/// A simple static top-level function type.
///
/// The invariant that this can be compiled as such is encoded in the fact that all
/// symbols it depends on are placed in it's cv-set.
#[derive(Debug, Clone, Eq)]
pub struct StaticFn {
    /// The pure function type associated with this static function type
    pure_fn: VarId<Pi>,
    /// The result representation of this static function type
    result: ReprId,
    /// The universe of this static function type
    ty: VarId<Universe>,
    /// The constant variable set == the free variable set of this static function type
    fv: SymbolSet,
}

impl StaticFn {
    /// Get the pure function type associated with this static function type
    pub fn pure_fn(&self) -> &VarId<Pi> {
        &self.pure_fn
    }
    /// Get the argument of this static function type
    pub fn arg(&self) -> &SymbolId {
        match self.pure_fn.as_enum() {
            ValueEnum::Pi(p) => p.arg(),
            v => panic!("Expected pi type, got {:#?} for static {:#?}", v, self),
        }
    }
}

impl PartialEq for StaticFn {
    #[inline]
    fn eq(&self, other: &StaticFn) -> bool {
        self.pure_fn.eq(&other.pure_fn) && self.result.eq(&other.pure_fn) && self.ty.eq(&other.ty)
    }
}

impl Hash for StaticFn {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.pure_fn.hash(hasher);
        self.result.hash(hasher);
        self.ty.hash(hasher)
    }
}

impl Typed for StaticFn {
    #[inline]
    fn repr(&self) -> &ReprId {
        self.ty.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        self.ty.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl MaybeType for StaticFn {}

impl MaybeRepr for StaticFn {
    #[inline(always)]
    fn is_repr(&self) -> bool {
        true
    }
    /// Attempt to match this representation for a substitution
    #[inline(always)]
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        let fail = || self.pure_fn.match_ty(other).map(Match::from);
        if let ValueEnum::StaticFn(other) = other.as_enum() {
            let arg_match = other.arg().repr().match_repr(self.arg().repr().as_val())?;
            if !arg_match.is_repr() {
                return fail();
            }
            let result_match = self.result.match_repr(other.result.as_val())?;
            if !result_match.is_repr() {
                return fail();
            }
            return Ok(arg_match & result_match);
        }
        fail()
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(Some(self.pure_fn.as_ty()))
    }
    /// If this is a representation, return the register it interprets if any
    #[inline(always)]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        //TODO: static function register kind...
        Ok(None)
    }
    /// Attempt to apply this value to a set of arguments as a function representation
    fn apply_repr(
        &self,
        mut arg_stack: ArgStack,
        ctx: &mut EvalCtx,
    ) -> Result<Option<ReprId>, Error> {
        let arg = if let Some(arg) = arg_stack.pop() {
            arg
        } else {
            return Ok(None);
        };
        let pure_fn = match self.pure_fn.as_enum() {
            ValueEnum::Pi(pure_fn) => pure_fn,
            v => panic!(
                "Expected pure function type to be a pi type, but got {:?}",
                v
            ),
        };
        let (match_, mut new_ctx) = ctx.inserted(pure_fn.arg().clone(), arg)?;
        let ctx = new_ctx.as_mut().unwrap_or(ctx);
        let sub = match match_ {
            Match::Repr(_) => &self.result,
            Match::Type(_) => pure_fn.result().as_repr(),
        };
        let result = sub
            .substitute(ctx)
            .map(ValId::coerce_unchecked)
            .unwrap_or_else(|| sub.clone());
        Ok(Some(result))
    }
}

impl MaybeReg for StaticFn {}

impl Value for StaticFn {
    fn into_enum(self) -> ValueEnum {
        ValueEnum::StaticFn(self)
    }
}

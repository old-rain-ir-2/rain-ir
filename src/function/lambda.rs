/*!
Lambda functions
*/
use super::*;

/// A lambda function
#[derive(Debug, Clone, Eq)]
pub struct Lambda {
    /// The symbol being abstracted over
    arg: SymbolId,
    /// The result
    result: ValId,
    /// The free variable set of this lambda function
    fv: SymbolSet,
    /// The type of this lambda function
    ty: VarId<Pi>,
}

impl Lambda {
    /// Create a new *abstract* lambda function from an argument and symbol
    pub fn new(arg: SymbolId, result: ValId) -> Lambda {
        Self::new_in(arg, result, &mut ())
    }
    /// Create a new pi type from an argument and symbol, with an optional consing context
    pub fn new_in<C: ConsCtx<SymbolId, ()>>(arg: SymbolId, result: ValId, ctx: &mut C) -> Lambda {
        let ty = VarId::from_var(Pi::new_in(arg.clone(), result.clone_ty(), ctx));
        Lambda::construct_lambda_in(arg, result, ty, ctx)
    }
    /// Construct a lambda function from an argument, result, and representation
    fn construct_lambda_in<C: ConsCtx<SymbolId, ()>>(
        arg: SymbolId,
        result: ValId,
        ty: VarId<Pi>,
        ctx: &mut C,
    ) -> Lambda {
        let result_fv = result.fv();
        let fv = result_fv
            .unioned_in(&arg.repr().fv(), ctx)
            .removed_in(&arg, ctx)
            .unwrap_or_else(|| result_fv.clone());
        Lambda {
            arg,
            result,
            ty,
            fv,
        }
    }
}

impl MaybeReg for Lambda {}

impl MaybeType for Lambda {}

impl MaybeRepr for Lambda {}

impl Typed for Lambda {
    #[inline]
    fn repr(&self) -> &ReprId {
        self.ty.as_repr()
    }
    #[inline]
    fn ty(&self) -> &TypeId {
        self.ty.as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<&RegId> {
        None
    }
}

impl Value for Lambda {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Lambda(self)
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    #[inline]
    fn substitute_norm(&self, ctx: &mut EvalCtx, normalize: bool) -> Option<ValId> {
        let mut weakened_ctx = ctx.removed(&self.arg);
        let active_ctx = weakened_ctx.as_mut().unwrap_or(ctx);
        let arg_repr = self.arg.repr().substitute(active_ctx);
        let result = self.result.substitute(active_ctx);
        if arg_repr.is_none() && result.is_none() {
            return None;
        }
        let arg = arg_repr
            .map(|repr| SymbolId::new(repr.coerce_unchecked()))
            .unwrap_or_else(|| self.arg.clone());
        let result = result.unwrap_or_else(|| self.result.clone());
        //TODO: proper repr
        let lambda = Lambda::new(arg, result);
        //TODO: eta conversion?
        if normalize && !ctx.normalize() {
            Some(lambda.normal_form().unwrap_or_else(|| lambda.into_val()))
        } else {
            Some(lambda.into_val())
        }
    }
    #[inline]
    fn normal_form(&self) -> Option<ValId> {
        if let Some(arg_repr) = self.arg.repr().normal_form() {
            let arg = SymbolId::new(arg_repr.coerce_unchecked());
            let mut ctx = EvalCtx::default();
            ctx.set_normalize(true);
            ctx.insert(self.arg.clone(), Symbol::from(arg.clone()).into_val())
                .expect("Valid substitution");
            let result = self
                .result
                .substitute(&mut ctx)
                .unwrap_or_else(|| self.result.clone());
            let lambda = Lambda::new(arg, result);
            Some(lambda.into_val())
        } else {
            self.result
                .normal_form()
                .map(|result| Lambda::new(self.arg.clone(), result).into_val())
        }
    }
    /// Apply this value to a set of arguments within a given evaluation context
    #[inline]
    fn apply(&self, mut arg_stack: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ValId>, Error> {
        let arg = if let Some(arg) = arg_stack.pop() {
            arg
        } else {
            return Ok(self.substitute(ctx));
        };
        let (_match, mut new_ctx) = ctx.inserted(self.arg.clone(), arg)?;
        let result = self
            .result
            .apply(arg_stack, new_ctx.as_mut().unwrap_or(ctx))?
            .unwrap_or_else(|| self.result.clone());
        Ok(Some(result))
    }
}

impl PartialEq for Lambda {
    #[inline]
    fn eq(&self, other: &Lambda) -> bool {
        // Why not generic over the particular arg?
        // 1) breaks Hash
        // 2) too complicated, bad performance: ergo, better as a helper
        self.arg == other.arg && self.result == other.result
    }
}

impl Hash for Lambda {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.arg.hash(hasher);
        self.result.hash(hasher);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::primitive::nats::*;

    #[test]
    fn abstract_identity_works() {
        let nats = &*NATS;
        let small_nats = &*SMALL_NATS;
        let x = SymbolId::new(nats.clone_as_repr());
        let id = Lambda::new(x.clone(), Symbol::from(x).into_val());
        assert_eq!(id.normal_form(), None);
        let mut ctx = EvalCtx::default();
        for small in small_nats.iter() {
            assert_eq!(
                id.apply(ArgStack::unary(small.clone_as_val()), &mut ctx)
                    .unwrap()
                    .unwrap(),
                *small
            );
        }
    }
}

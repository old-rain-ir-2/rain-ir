/*!
Pi types
*/
use super::*;
use std::borrow::Borrow;

/// A pi type
#[derive(Debug, Clone, Eq)]
pub struct Pi {
    /// The symbol being abstracted over
    arg: SymbolId,
    /// The target representation
    result: TypeId,
    /// The free variable set of this pi type
    fv: SymbolSet,
}

impl Pi {
    /// Create a new pi type from an argument and symbol
    pub fn new(arg: SymbolId, result: TypeId) -> Pi {
        Self::new_in(arg, result, &mut ())
    }
    /// Create a new function type
    ///
    /// # Examples
    /// ```rust
    /// # use rain_ir::function::Pi;
    /// # use rain_ir::primitive::nats::*;
    /// # use rain_ir::value::*;
    /// // nats -> nats
    /// let unary = Pi::function(Nats.into_ty(), Nats.into_ty());
    /// // nats -> nats -> nats
    /// let binary = Pi::function(Nats.into_ty(), unary.clone().into_ty());
    /// ```
    pub fn function(arg: TypeId, result: TypeId) -> Pi {
        Self::function_in(arg, result, &mut ())
    }
    /// Create a new pi type from an argument and symbol, with an optional consing context
    pub fn new_in<C: ConsCtx<SymbolId, ()>>(arg: SymbolId, result: TypeId, ctx: &mut C) -> Pi {
        let result_fv = result.fv();
        let fv = result_fv
            .unioned_in(&arg.repr().fv(), ctx)
            .removed_in(&arg, ctx)
            .unwrap_or_else(|| result_fv.clone());
        Pi { arg, result, fv }
    }
    /// Create a new function type from an argument and symbol, with an optional consing context
    pub fn function_in<C: ConsCtx<SymbolId, ()>>(arg: TypeId, result: TypeId, ctx: &mut C) -> Pi {
        Self::new_in(SymbolId::new(arg.into_repr()), result, ctx)
    }
    /// Get the argument of this pi type
    pub fn arg(&self) -> &SymbolId {
        &self.arg
    }
    /// Get the result type of this pi type
    pub fn result(&self) -> &TypeId {
        &self.result
    }
}

impl MaybeReg for Pi {}

impl MaybeRepr for Pi {
    #[inline(always)]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline]
    fn match_repr(&self, other: &ValId) -> Result<Match, Error> {
        self.match_ty(other.try_repr_ty()?.map(TypeId::as_val).unwrap_or(other))
            .map(Match::Type)
    }
    #[inline(always)]
    fn try_repr_ty(&self) -> Result<Option<&TypeId>, Error> {
        Ok(None)
    }
    #[inline(always)]
    fn try_repr_reg(&self) -> Result<Option<&RegId>, Error> {
        Ok(None)
    }
    fn apply_repr(&self, mut args: ArgStack, ctx: &mut EvalCtx) -> Result<Option<ReprId>, Error> {
        let arg = if let Some(arg) = args.pop() {
            arg
        } else {
            return Ok(self.substitute(ctx).map(|repr| repr.coerce_unchecked()));
        };
        let (_m, mut new_ctx) = ctx.inserted(self.arg.clone(), arg.borrow().clone())?;
        let result = self
            .result
            .apply_repr(args, new_ctx.as_mut().unwrap_or(ctx))?
            .unwrap_or_else(|| self.result.clone_as_repr());
        Ok(Some(result))
    }
}

impl Type for Pi {}

impl MaybeType for Pi {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn match_ty(&self, other: &ValId) -> Result<TypeMatch, Error> {
        let other = match other.as_enum() {
            ValueEnum::Pi(other) => other,
            _ => {
                return if let Some(other) = other.normal_form() {
                    self.match_ty(&other)
                } else {
                    Err(Error::TypeMismatch(
                        "Nats is not a subtype of any other type",
                    ))
                }
            }
        };
        //TODO: think about argument equality and alpha-conversion
        let arg_match = other.arg.repr().match_ty(self.arg.repr().as_val())?;
        let result_match = self.result.match_ty(other.result.as_val())?;
        Ok(arg_match & result_match)
    }
}

impl Typed for Pi {
    #[inline]
    fn ty(&self) -> &TypeId {
        //TODO: take union of result universe and symbol universe!
        self.result.ty()
    }
    #[inline]
    fn repr(&self) -> &ReprId {
        //TODO: take union of result universe and symbol universe!
        self.result.repr()
    }
}

impl Value for Pi {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Pi(self)
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    #[inline]
    fn substitute_norm(&self, ctx: &mut EvalCtx, normalize: bool) -> Option<ValId> {
        let mut weakened_ctx = ctx.removed(&self.arg);
        let active_ctx = weakened_ctx.as_mut().unwrap_or(ctx);
        let arg_repr = self.arg.repr().substitute(active_ctx);
        let result = self.result.substitute(active_ctx);
        if arg_repr.is_none() && result.is_none() {
            return None;
        }
        let arg = arg_repr
            .map(|repr| SymbolId::new(repr.coerce_unchecked()))
            .unwrap_or_else(|| self.arg.clone());
        let result = result
            .map(|repr| repr.coerce_unchecked())
            .unwrap_or_else(|| self.result.clone());
        let pi = Pi::new(arg, result);
        if normalize && !ctx.normalize() {
            Some(pi.normal_form().unwrap_or_else(|| pi.into_val()))
        } else {
            Some(pi.into_val())
        }
    }
    #[inline]
    fn normal_form(&self) -> Option<ValId> {
        let arg_repr = self.arg.repr().normal_form();
        let result = self.result.normal_form();
        if arg_repr.is_none() && result.is_none() {
            return None;
        }
        let arg = arg_repr
            .map(|repr| SymbolId::new(repr.coerce_unchecked()))
            .unwrap_or_else(|| self.arg.clone());
        let result = result
            .map(|repr| repr.coerce_unchecked())
            .unwrap_or_else(|| self.result.clone());
        Some(Pi::new(arg, result).into_val())
    }
}

impl PartialEq for Pi {
    #[inline]
    fn eq(&self, other: &Pi) -> bool {
        // Why not generic over the particular arg?
        // 1) breaks Hash
        // 2) too complicated, bad performance: ergo, better as a helper
        self.arg == other.arg && self.result == other.result
    }
}

impl Hash for Pi {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.arg.hash(hasher);
        self.result.hash(hasher);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eval::EvalCtx;
    use crate::primitive::nats::*;

    #[test]
    fn pi_apply_repr_sanity_checl() {
        let nats = &*NATS;
        let zero = &SMALL_NATS[0];
        let unary = Pi::function(nats.clone_as_ty(), nats.clone_as_ty()).into_ty();
        let mut ctx = EvalCtx::default();
        assert_eq!(
            unary
                .apply_repr(ArgStack::unary(zero.clone_as_val()), &mut ctx)
                .unwrap()
                .unwrap(),
            *nats
        );
        let binary = Pi::function(nats.clone_as_ty(), unary.clone());
        assert_eq!(
            binary
                .apply_repr(ArgStack::unary(zero.clone_as_val()), &mut ctx)
                .unwrap()
                .unwrap(),
            unary
        );
        assert_eq!(
            binary
                .apply_repr(
                    ArgStack::binary(zero.clone_as_val(), zero.clone_as_val()),
                    &mut ctx
                )
                .unwrap()
                .unwrap(),
            *nats
        );
    }
}
